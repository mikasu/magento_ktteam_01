<?php

namespace Pfay\Contacts\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;


class Test extends AbstractModel
{

    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\KTteam\Test\Model\ResourceModel\Test::class);
    }

}