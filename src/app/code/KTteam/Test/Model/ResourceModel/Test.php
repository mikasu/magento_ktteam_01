<?php

namespace KTteam\Test\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Contact extends AbstractDb
{

    public function _construct()
    {
        $this->_init('module_test_table', 'module_test_table_id');
    }
}